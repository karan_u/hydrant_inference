import sys
import time
import numpy as np
import tensorflow as tf
import cv2
from os import listdir, system
from os.path import isfile, join
import os
from utils import label_map_util
from utils import visualization_utils as vis_util
from threading import Thread
import json
import io
import datetime
from PIL import Image
import csv
import pandas as pd
import piexif as px
ORIGIN_DIR = 'input_images/'
IMG_DIR = 'split_images/'
file_list = [f for f in listdir(IMG_DIR) if isfile(join(IMG_DIR, f)) and (f.endswith('.jpeg') or f.endswith('.jpg'))]

class fire_hydrant_service():
    
    def __init__(self):
        # pwd = os.getcwd()
        # command = 'source ' + os.path.join(pwd,'tf_2','bin','activate')
        # system(command)
        # abs_path = os.path.join(pwd, 'tf_2/lib/python3.7/site-packages/tensorflow/models/research')
        # command = "export PYTHONPATH=$PYTHONPATH:"+ abs_path +":" + abs_path + "/slim"
        # system(command)
        self.PATH_TO_CKPT = os.path.join('model', 'frozen_inference_graph.pb')
        # List of the strings that is used to add correct label for each box.
        self.PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
        self.NUM_CLASSES = 1
        #self.IMG_DIR = "OID/Dataset/validation/test_images"
        #self.WRITE_DIR = "annotated_media/"
        #self.CROPPED_WRITE_DIR = "cropped_images/"
        self.label_map = label_map_util.load_labelmap(self.PATH_TO_LABELS)
        self.categories = label_map_util.convert_label_map_to_categories(self.label_map, max_num_classes=self.NUM_CLASSES, use_display_name=True)
        self.category_index = label_map_util.create_category_index(self.categories)
        self.detection_graph = tf.Graph()
        self.import_graph()
        
        

    def import_graph(self):
        with self.detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(self.PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                #print(fid.read())
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
             
    def load_image_into_numpy_array(self,image):
        #print(image.size)
        #print(image.format)
        if image.format == "PNG" or image.format=="JPEG":image = image.convert('RGB')
        (im_width, im_height) = image.size
        return np.array(image.getdata()).reshape((im_height, im_width,3)).astype(np.uint8)

    def bounding_predict(self,image_path):
        print("--------------------------------------------------------------------------------------------------------------")
        #print(image_path)
        image=Image.open(image_path)
        exif_data = px.load(image_path)
        gps_data = exif_data['GPS'] #https://sno.phy.queensu.ca/~phil/exiftool/TagNames/GPS.html https://gis.stackexchange.com/questions/136925/how-to-parse-exif-gps-information-to-lat-lng-decimal-numbers
        #print(gps_data)
        gps_lat = gps_data[2]
        degrees = float(gps_lat[0][0]) / float(gps_lat[0][1]) #deg numerator/ lat denominator
        #print(degrees)
        minutes = float(gps_lat[1][0]) / float(gps_lat[1][1])
        #print(minutes)
        seconds = float(gps_lat[2][0]) / float(gps_lat[2][1])
        #print(seconds)

        float_lat = degrees + minutes/60 + seconds/3600

        gps_long = gps_data[4]
        degrees = float(gps_long[0][0]) / float(gps_long[0][1]) #deg numerator/ deg denominator
        minutes = float(gps_long[1][0]) / float(gps_long[1][1])
        seconds = float(gps_long[2][0]) / float(gps_long[2][1])

        float_long = degrees + minutes/60 + seconds/3600

        #self.import_graph()
        with self.detection_graph.as_default():
            config = tf.compat.v1.ConfigProto()
            #config.gpu_optionsimage_path.allow_growth = True
            with tf.compat.v1.Session(graph=self.detection_graph, config=config) as sess:
                image_np = image
                image_np = self.load_image_into_numpy_array(image)
                (im_width, im_height, im_depth) = image_np.shape
                if im_depth !=3 :
                    print("The depth of the image is not 3")
                else:
                    # the array based representation of the image will be used later in order to prepare the
                    # result image with boxes and labels on it.
                    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
                    # Each box represents a part of the image where a particular object was detected.
                    boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
                    # Each score represent how level of confidence for each of the objects.
                    # Score is shown on the result image, together with the class label.
                    scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
                    classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
                    num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
                    # Actual detection.
                    
                    #start_time = time.time()
                    (boxes, scores, classes, num_detections) = sess.run([boxes, scores, classes, num_detections],feed_dict={image_tensor: image_np_expanded})
                    num_boxes = 0
                    #print(boxes)
                    for i in range(100):
                        if scores[0][i] > 0.7:
                            num_boxes += 1
                    #print(boxes[0][0:num_boxes])
                    #print(np.squeeze(boxes))
                    #elapsed_time = time.time() - start_time
                    #print('inference time cost: {}'.format(elapsed_time))
                    
                    #image_id=message['key'].split(".")[0]
                    #print(num_boxes)
                    flag = False

                    for i in range(num_boxes):
                        print(int(classes[0][i]))
                        if int(classes[0][i]) == 11:
                            flag = True
                    
                    if flag == False:
                        inference = {}
                        return inference
                    if num_boxes==1:
                        inference={}
                    # inference["image_id"]=image_id
                     #   inference['hydrant_id']=str(uuid.uuid1())
                    # inference['bucket'] = message['bucket']
                        print(ORIGIN_DIR + image_path.split('/')[1].split('__')[0])
                        inference['Sphere_name'] = ORIGIN_DIR + image_path.split('/')[1].split('__')[0] + '.jpg'
                        inference['Image_name'] = image_path
                        inference["box_dim"]={"y1":boxes[0][0][0]*im_height,
                        "x1":boxes[0][0][1]*im_width,
                        "y2":boxes[0][0][2]*im_height,
                        "x2":boxes[0][0][3]*im_width
                        }
                        inference['geo_lat'] = float_lat
                        inference['geo_long'] = float_long
                        print(inference)
                        return inference
                    else:
                        list_json=[]
                        for i in range(0,len(boxes[0][0:num_boxes])):
                            if int(classes[0][i]) == 11:
                                inference={}
                                #print(boxes[0][i])
                                #inference["image_id"]=image_id
                                #inference['hydrant_id']=str(uuid.uuid4())
                                inference['Image_name'] = image_path
                                inference["box_dim"]={"y1":boxes[0][i][0]*im_height,
                                "x1":boxes[0][i][1]*im_width,
                                "y2":boxes[0][i][2]*im_height,
                                "x2":boxes[0][i][3]*im_width
                                }
                                inference['geo_lat'] = float_lat
                                inference['geo_long'] = float_long
                                print(ORIGIN_DIR + image_path.split('/')[1].split('__')[0])
                                inference['Sphere_name'] = ORIGIN_DIR + image_path.split('/')[1].split('__')[0] + '.jpg'
                                list_json.append(inference)
                        print(list_json)
                       # print(boxes)
                        return list_json
fc = fire_hydrant_service()                          
def addToFile(file, what):
    f = open(file, 'a').write(what) 


# def draw_box(self,image,image_path):
#     image = Image.open(image_path)
#     # the array based representation of the image will be used later in order to prepare the
#     # result image with boxes and labels on it.
#     image_np = load_image_into_numpy_array(image)
#     # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
#     image_np_expanded = np.expand_dims(image_np, axis=0)
#     # Actual detection.
#     output_dict = run_inference_for_single_image(image_np_expanded, detection_graph)
#     # Visualization of the results of a detection.
#     vis_util.visualize_boxes_and_labels_on_image_array(
#         image_np,
#         output_dict['detection_boxes'],
#         output_dict['detection_classes'],
#         output_dict['detection_scores'],
#         category_index,
#         instance_masks=output_dict.get('detection_masks'),
#         use_normalized_coordinates=True,
#         line_thickness=8)
#     plt.figure(figsize=IMAGE_SIZE)
#     plt.imshow(image_np)
            
def main(image):
    print("main function")

    list_of_json = fc.bounding_predict(IMG_DIR+image)
    print(list_of_json)
    if bool(list_of_json) == True:
        df = pd.io.json.json_normalize(list_of_json, meta=['Sphere_name','Image_name','geo_lat', 'geo_long', ['box_dim', 'y1', 'x1', 'y2', 'x2']])
        df = df.reindex(columns=['Sphere_name','Image_name','geo_lat', 'geo_long', 'box_dim.y1', 'box_dim.x1', 'box_dim.y2', 'box_dim.x2',])
        with open('annotaions1.csv', 'a') as f:
            df.to_csv(f,header=False, sep=',',index= False, encoding='utf-8')

if __name__ == "__main__":

    for img in file_list:
        main(img)