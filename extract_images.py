import numpy as np
import cv2
import os
from os import listdir
from os.path import isfile, join
import sys
import piexif as px
from PIL import Image

# def s3up(file_name, bucket_name):
#     #metadata = {"Metadata": {"geoloc": str(g.latlng), "sensorid": "11e9-bbf1-acde48001122", "timestamp": datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f")} }
#     #file_name = "vid_frames/frame" + str(count) + ".jpg"
#     #s3_file_name =  "input-frames/" + "11e9-bbf1-acde48001122" + "__" + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f") + ".jpg"
#     #print(s3_file_name)
#     #print("Starting upload of frame number : " + str(count))
#     s3.upload_file(file_name, bucket_name, s3_file_name, ExtraArgs = metadata)
#     #print("Finishing upload of frame number : " + str(count))


NUM_IMAGES = 5 # number of images stitched into one panorama
IMG_DIR = "input_images"
WRITE_DIR = "split_images/"
file_list = [f for f in listdir(IMG_DIR) if isfile(join(IMG_DIR, f)) and (f.endswith('.jpeg') or f.endswith('.jpg'))]
num_files = len(file_list)
for image_index in range(num_files):
    file_name = os.path.join(IMG_DIR, file_list[image_index])
    img = Image.open(file_name)
    exif_data = px.load(file_name)
    #print(dict1['GPS'][1].decode('utf-8'))

    gps_data = exif_data['GPS'] #https://sno.phy.queensu.ca/~phil/exiftool/TagNames/GPS.html https://gis.stackexchange.com/questions/136925/how-to-parse-exif-gps-information-to-lat-lng-decimal-numbers
    gps_lat = gps_data[2]
    degrees = gps_lat[0][0] / gps_lat[0][1] #deg numerator/ lat denominator
    minutes = gps_lat[1][0] / gps_lat[1][1]
    seconds = gps_lat[2][0] / gps_lat[2][0]

    float_lat = degrees + minutes/60 + seconds/3600

    gps_long = gps_data[4]
    degrees = gps_long[0][0] / gps_long[0][1] #deg numerator/ deg denominator
    minutes = gps_long[1][0] / gps_long[1][1]
    seconds = gps_long[2][0] / gps_long[2][0]

    float_long = degrees + minutes/60 + seconds/3600
    
    gps_meta = str(float_lat) + "#" + str(float_long)
    print(gps_meta)
    metadata = {"Metadata": {"geoloc": gps_meta} }
    img_width, img_height = img.size
    separated_img_width = int(img_width/NUM_IMAGES)
    img = np.array(img)
    for img_num in range(NUM_IMAGES):
        new_img = img[:, separated_img_width*img_num : separated_img_width*(img_num+1), :]
        #Upload to S3 here
        exif_bytes = px.dump(exif_data)
        fname = WRITE_DIR + file_list[image_index].split('.')[0] + '__' + str(img_num) + '.jpg'
        new_img = Image.fromarray(new_img)
        new_img.save(fname, exif=exif_bytes)